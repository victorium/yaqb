<?php

use Yaqb\Expression\JoinExpression;
use Yaqb\Query;

class QueryTest extends PHPUnit_Framework_TestCase {
	public function testConstruction() {
		$query = new Query();
		$this->assertInstanceOf("\Yaqb\Query", $query);
	}

	public function testConstructionParameterNamespace() {
		$query = new Query("stt_");
		$this->assertInstanceOf("\Yaqb\Query", $query);
		$this->assertEquals($query->getParameterNamespace(), "stt_");
	}

	public function testMode() {
		$query = new Query();
		$query->setMode(Query::SELECT);
		$this->assertEquals($query->getMode(), Query::SELECT);
	}

	public function testTable() {
		$query = new Query();
		$query->setTable("simple");
		$this->assertEquals($query->getTable(), "simple");
	}

	public function testTableWithAlias() {
		$query = new Query();
		$query->setTable("simple", "s");
		$this->assertEquals($query->getTable(), "simple AS s");
	}

	public function testBadJoin() {
		$query = new Query();
		try {
			$query->addJoin(new JoinExpression("simple AS s", JoinExpression::INNER_JOIN, JoinExpression::ON_CONDITION, "s.id=1"));
			$this->assertEquals(1, 2, "Should not be here");
		} catch (\Exception $err) {
			$this->assertEquals("Join can't come before setting table", $err->getMessage());
		}
	}

	public function testBadHaving() {
		$query = new Query();
		$query->setTable("simple");
		$query->setMode(Query::SELECT);
		try {
			$query->setHaving("b_sum > 70");
			$this->assertEquals(1, 2, "Should not be here");
		} catch (\Exception $err) {
			$this->assertEquals("Missing group by clause before having keyword", $err->getMessage());
		}
	}

	public function testDefaultValues() {
		$query = new Query();
		$query->setDefaultValues(true);
		$this->assertEquals($query->getDefaultValues(), true);
		$query->setDefaultValues(false);
		$this->assertEquals($query->getDefaultValues(), false);
	}
}
