<?php

use Yaqb\Expression\JoinExpression;

class JoinExpressionTest extends PHPUnit_Framework_TestCase {
	public function testGetSqlForUsing() {
		$joinExpression = new JoinExpression("simple", JoinExpression::INNER_JOIN, JoinExpression::USING_CONDITION, "b, id");
		$this->assertInstanceOf("\Yaqb\Expression\JoinExpression", $joinExpression);
		$this->assertEquals($joinExpression->sql, "INNER JOIN simple USING (b, id) ");
	}
}
