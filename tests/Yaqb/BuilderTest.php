<?php

use Yaqb\Builder;

class BuilderTest extends PHPUnit_Framework_TestCase {
	public function testConstruction() {
		$builder = new Builder();
		$this->assertInstanceOf("\Yaqb\Builder", $builder);

		$builder = Builder::init();
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
	}

	public function testConstructionParameterNamespace() {
		$builder = new Builder("stt_");
		$this->assertInstanceOf("\Yaqb\Builder", $builder);

		$builder = Builder::init("stt_");
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
	}

	public function testBlank() {
		$builder = Builder::init();
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testSimpleInsert() {
		$builder = Builder::init()->insert("pointer")->values(["a" => 20, "b" => 40]);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a, b) VALUES (:a_0, :b_0)");
		$this->assertEquals($builder->getParameters(), ["a_0" => 20, "b_0" => 40]);
	}

	public function testSimpleInsertReturning() {
		$builder = Builder::init()->insert("pointer")->values(["a" => 20, "b" => 40]);
		$builder->getQuery()->setReturning("id");
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a, b) VALUES (:a_0, :b_0) RETURNING id");
		$this->assertEquals($builder->getParameters(), ["a_0" => 20, "b_0" => 40]);
	}

	public function testSimpleInsertNamespace() {
		$builder = Builder::init("shp")->insert("pointer")->values(["a" => 20, "b" => 40]);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a, b) VALUES (:shp_a_0, :shp_b_0)");
		$this->assertEquals($builder->getParameters(), ["shp_a_0" => 20, "shp_b_0" => 40]);
	}

	public function testSimpleInsertDefaultValues() {
		$builder = Builder::init("shp")->insert("pointer")->values(["a" => 20]);
		$builder->getQuery()->setDefaultValues(true);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a) VALUES (:shp_a_0) DEFAULT VALUES");
		$this->assertEquals($builder->getParameters(), ["shp_a_0" => 20]);
	}

	public function testSimpleMultipleInsert() {
		$builder = Builder::init()->insert("pointer")->values(["a" => 20, "b" => 40])->values(["a" => 10, "b" => 30]);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a, b) VALUES (:a_0, :b_0), (:a_1, :b_1)");
		$this->assertEquals($builder->getParameters(), ["a_0" => 20, "b_0" => 40, "a_1" => 10, "b_1" => 30]);
	}

	public function testRawInsert() {
		$builder = Builder::init()->insert("pointer")->values(["a" => 20, "b" => ["(SELECT b FROM setter WHERE id=4)"]]);
		$this->assertEquals($builder->getSql(), "INSERT INTO pointer (a, b) VALUES (:a_0, (SELECT b FROM setter WHERE id=4))");
		$this->assertEquals($builder->getParameters(), ["a_0" => 20]);
	}

	public function testSimpleSelect() {
		$builder = Builder::init()->select("pointer");
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer");
	}

	public function testSimpleSelectLimit() {
		$builder = Builder::init()->select("pointer")->limit(10);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer LIMIT 10");
	}

	public function testSimpleSelectLimitOffset() {
		$builder = Builder::init()->select("pointer")->limit(10)->offset(10);
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer LIMIT 10 OFFSET 10");
	}

	public function testSimpleSelectBadOffset() {
		try {
			$builder = Builder::init()->select("pointer")->offset(10);
			$this->assertEquals(1, 2, "Should not be here");
		} catch (\Exception $err) {
			$this->assertEquals("Missing limit clause before offset keyword", $err->getMessage());
		}
	}

	public function testSimpleSelectOrderBy() {
		$builder = Builder::init()->select("pointer")->orderBy("b");
		$this->assertInstanceOf("\Yaqb\Builder", $builder);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer ORDER BY b");
	}

	public function testSimpleSelectSimpleWhere() {
		$builder = Builder::init()->select("pointer")->where("id", 4);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id=:id_0");
		$this->assertEquals($builder->getParameters(), ["id_0" => 4]);
	}

	public function testSimpleSelectInWhere() {
		$builder = Builder::init()->select("pointer")->where("id", [4, 5, 7]);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id IN (:id_0_0, :id_0_1, :id_0_2)");
		$this->assertEquals($builder->getParameters(), ["id_0_0" => 4, "id_0_1" => 5, "id_0_2" => 7]);
	}

	public function testSimpleSelectOredWhere() {
		$builder = Builder::init()->select("pointer")->where("id", 4)->orCondition()->where("id", 5);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id=:id_0 OR id=:id_1");
		$this->assertEquals($builder->getParameters(), ["id_0" => 4, "id_1" => 5]);
	}

	public function testSimpleSelectScopedWhere() {
		$builder = Builder::init()->select("pointer")
												->subWhere()
													->where("id", 5)
													->where("b", 50)
												->subWhereEnd();
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE (id=:id_0 AND b=:b_1)");
		$this->assertEquals($builder->getParameters(), ["id_0" => 5, "b_1" => 50]);
	}

	public function testSimpleSelectScopedWhere2() {
		$builder = Builder::init()->select("pointer")->where("id", 4)
												->orCondition()
												->subWhere()
													->where("id", 5)
													->andCondition()
													->where("b", 50)
												->subWhereEnd();
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id=:id_0 OR (id=:id_1 AND b=:b_2)");
		$this->assertEquals($builder->getParameters(), ["id_0" => 4, "id_1" => 5, "b_2" => 50]);
	}

	public function testSimpleSelectRawWhere() {
		$builder = Builder::init()->select("pointer")->whereRaw("id=(SELECT id FROM setter WHERE b=40)");
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id=(SELECT id FROM setter WHERE b=40)");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testSimpleSelectGroupBy() {
		$builder = Builder::init()->select("pointer")->column("SUM(b)", "b_sum")->groupBy("id");
		$this->assertEquals($builder->getSql(), "SELECT SUM(b) AS b_sum FROM pointer GROUP BY id");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testSimpleSelectGroupByHaving() {
		$builder = Builder::init()->select("pointer")->column("SUM(b)", "b_sum")->groupBy("id")->having("b_sum > 70");
		$this->assertEquals($builder->getSql(), "SELECT SUM(b) AS b_sum FROM pointer GROUP BY id HAVING b_sum > 70");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testSimpleUpdate() {
		$builder = Builder::init()->update("pointer")->values(["id" => 40]);
		$this->assertEquals($builder->getSql(), "UPDATE pointer SET id=:id");
		$this->assertEquals($builder->getParameters(), ["id" => 40]);
	}

	public function testUpdateRawNamespaced() {
		$builder = Builder::init("nms")->update("pointer")->values(["id" => ["id+1"], "b" => 40])->where("b", 100, ">");
		$this->assertEquals($builder->getSql(), "UPDATE pointer SET id=id+1, b=:nms_b WHERE b>:nms_b_0");
		$this->assertEquals($builder->getParameters(), ["nms_b" => 40, "nms_b_0" => 100]);
	}

	public function testSimpleUpdateWhere() {
		$builder = Builder::init()->update("pointer")->values(["id" => 40])->where("b", 40);
		$this->assertEquals($builder->getSql(), "UPDATE pointer SET id=:id WHERE b=:b_0");
		$this->assertEquals($builder->getParameters(), ["id" => 40, "b_0" => 40]);
	}

	public function testSimpleDelete() {
		$builder = Builder::init()->delete("pointer");
		$this->assertEquals($builder->getSql(), "DELETE FROM pointer");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testSimpleDeleteWhere() {
		$builder = Builder::init()->delete("pointer")->where("id", 17);
		$this->assertEquals($builder->getSql(), "DELETE FROM pointer WHERE id=:id_0");
		$this->assertEquals($builder->getParameters(), ["id_0" => 17]);
	}

	public function testAddParameters() {
		$builder = Builder::init()->select("pointer")->whereRaw("id=(SELECT id FROM setter WHERE b=:b)")->addParameters(["b" => 40]);
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer WHERE id=(SELECT id FROM setter WHERE b=:b)");
		$this->assertEquals($builder->getParameters(), ["b" => 40]);
	}

	public function testInnerJoin() {
		$builder = Builder::init()->select("pointer")->innerJoin("polygon", "pointer_id=id");
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer INNER JOIN polygon ON pointer_id=id");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testLeftJoin() {
		$builder = Builder::init()->select("pointer")->leftJoin("polygon", "pointer_id=id");
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer LEFT JOIN polygon ON pointer_id=id");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testLeftOuterJoin() {
		$builder = Builder::init()->select("pointer")->leftOuterJoin("polygon", "pointer_id=id");
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer LEFT OUTER JOIN polygon ON pointer_id=id");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testInnerJoinAliases() {
		$builder = Builder::init()->select("pointer AS p1")->innerJoin("polygon AS p2", "p1.pointer_id=p2.id");
		$this->assertEquals($builder->getSql(), "SELECT * FROM pointer AS p1 INNER JOIN polygon AS p2 ON p1.pointer_id=p2.id");
		$this->assertEquals($builder->getParameters(), []);
	}

	public function testGetQuery() {
		$builder = Builder::init();
		$this->assertInstanceOf("\Yaqb\Query", $builder->getQuery());
	}

	public function testSelectColumn() {
		$builder = Builder::init()->select()->column("1+2", "result");
		$this->assertEquals($builder->getSql(), "SELECT 1+2 AS result");
	}

	public function testSelectMultipleColumns() {
		$builder = Builder::init()->select()->column("1+2", "sum")->column("2*2", "multiplication");
		$this->assertEquals($builder->getSql(), "SELECT 1+2 AS sum, 2*2 AS multiplication");
	}

	public function testSelectExists() {
		$subBuilder = Builder::init("sub")->select("pointer")->column("1")->where("id", 4);
		$builder = Builder::init()->select()->action("EXISTS")->subQuery($subBuilder->getSql(), "p_exists");
		$this->assertEquals($builder->getSql(), "SELECT EXISTS (SELECT 1 FROM pointer WHERE id=:sub_id_0) AS p_exists");
		$this->assertInstanceOf("\Yaqb\Query", $builder->getQuery());
	}
}
