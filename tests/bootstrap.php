<?php

spl_autoload_register(function($class) {
    if (strpos($class, "Yaqb\\") === 0) {
        $dir = strcasecmp(substr($class, -4), "Test") ? "src/" : "tests/";
        require __DIR__ . "/../" . $dir . strtr($class, "\\", DIRECTORY_SEPARATOR) . ".php";
    }
});
