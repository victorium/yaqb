<?php

spl_autoload_register(function($class) {
    if (strpos($class, "Yaqb\\") === 0) {
        require __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
    }
});
