<?php

/*
 * Yaqb/Builder.php
 *
 * Copyright: (c) 2015 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yaqb;

use Yaqb\Expression\JoinExpression;

/**
 * Class that allows chaining of different parts of the query.
 */
class Builder {
	/* The current query being built */
	protected $query = null;

	/* The current query being built */
	protected $whereConcatenator = "AND";
	protected $whereCounter = 0;

	public function __construct($parameterNamespace = "") {
		$this->query = new Query($parameterNamespace);
	}

	/**
	 * Method to allow easier chaining of queries, e.g:
	 *
	 * Builder::init()->select("book");
	 *
	 * @param string $namespace The prefix namespace for all parameters, default ""
	 * @return object Instance for chaining methods
	 */
	public static function init($parameterNamespace = "") {
		return new static($parameterNamespace);
	}

	/**
	 * @return Yaqb\Query object of the query being used by this builder
	 */
	 public function getQuery() {
		return $this->query;
	}

	/**
	 * Insert for specific table
	 *
	 * Builder::init()->insert("book")->values(["title"=>"The Helps", "author"=> "Kathryn Stockett"]);
	 *
	 * @param $table string The name of the table to work on. Can include name of schema
	 * @param string|null $alias The alias of the table to work on.
	 * @return object Instance for chaining methods
	 */
	public function insert($table, $alias = null) {
		$this->query->setMode(Query::INSERT);
		$this->query->setTable($table, $alias);
		return $this;
	}

	/**
	 * Update for specific table
	 *
	 * Builder::init()->update("book")->values(["title"=>"The Help"])->where("id", 1);
	 *
	 * @param $table string The name of the table to work on. Can include name of schema
	 * @param string|null $alias The alias of the table to work on.
	 * @return object Instance for chaining methods
	 */
	public function update($table, $alias = null) {
		$this->query->setMode(Query::UPDATE);
		$this->query->setTable($table, $alias);
		return $this;
	}

	/**
	 * Select for specific table
	 *
	 * Builder::init()->select("book", "title")->where("id", 1)
	 *
	 * @param string $table The name of the table to work on. Can include name of schema
	 * @param string|null $alias The alias of the table to work on.
	 * @return object Instance for chaining methods
	 */
	public function select($table = null, $alias = null) {
		$this->query->setMode(Query::SELECT);
		$this->query->setTable($table, $alias);
		return $this;
	}

	/**
	 * Delete from specific table
	 *
	 * Builder::init()->delete("book")->where("id", 1);
	 *
	 * @param string $table The name of the table to work on. If string is a can include name of schema
	 * @param string|null $alias The alias of the table to work on.
	 * @param string $resultColumns Optionally set the result column to return, "*" by default
	 * @return object Instance for chaining methods
	 */
	public function delete($table, $alias = null) {
		$this->query->setMode(Query::DELETE);
		$this->query->setTable($table, $alias);
		return $this;
	}

	/**
	 * Set extra directives directly after select, insert, update and delete keywords.
	 * These are usually vendor specific.
	 *
	 * Builder::init()->select("book")->action("DISTINCT author");
	 * Builder::init()->select("book")->action("DISTINCT author");
	 *
	 * @param string $action The directive such as DISTINCT, ALL etc
	 */
	function action($action) {
		$this->query->setAction($action);
		return $this;
	}

	/**
	 * Add a value to the result columns
	 *
	 * Builder::init()->select()->column("1");
	 * Builder::init()->select()->column("1", "num");
	 *
	 * @param string $column The table column or expression
	 * @param string $alias The alias of the column or expression
	 */
	function column($column, $alias = null) {
		if ($alias != null) {
			$column = "{$column} AS {$alias}";
		}
		$this->query->addResultColumns($column);
		return $this;
	}

	/**
	 * Add a value to the result columns
	 *
	 * Builder::init()->select()->subQuery("SELECT 1");
	 * Builder::init()->select()->subQuery("SELECT 1", "num");
	 *
	 * @param string $sql The sub SQL expression
	 * @param string $alias The alias of the SQL
	 */
	function subQuery($sql, $alias = null) {
		return $this->column("({$sql})", $alias);
	}

	/**
	 * Add limit to the query
	 *
	 * Builder::init()->select("book")->limit(10);
	 *
	 * @param $limit mixed String epression or integer to limit by
	 * @return object Instance for chaining methods
	 */
	public function limit($limit) {
		$this->query->setLimit($limit);
		return $this;
	}

	/**
	 * Add offset to the query
	 *
	 * Builder::init()->select("book")->limit(10)->offset(20);
	 *
	 * @param $offset mixed String expression or integer to offset by
	 * @return object Instance for chaining methods
	 */
	public function offset($offset) {
		$this->query->setOffset($offset);
		return $this;
	}

	/**
	 * Add where to the query
	 *
	 * Builder::init()->select("book")->where("id", 1);
	 * Builder::init()->delete("book")->where("id", 1, ">");
	 *
	 * @param string $key String expression or column name
	 * @param mixed $value Column value
	 * @param string $comparison The operator to use in comparison between $key and $value, defaults '=' and 'IN' for arrays
	 * @return object Instance for chaining methods
	 */
	public function where($key, $value, $comparison = null) {
		$flat = is_array($value); // for IN Clause

		if ($comparison === null) {
			$comparison = ($flat) ? "IN" : "=";
		}

		$parameterNamespace = $this->query->getParameterNamespace();
		$parameterNamespace = ($parameterNamespace !== "") ? $parameterNamespace . "_" : $parameterNamespace;

		if ($flat) {
			$paramsArray = [];
			$parameters = [];
			for ($i=0, $len=count($value); $i<$len; $i++) {
				$parameterKey = "{$parameterNamespace}{$key}_{$this->whereCounter}_{$i}";
				$paramsArray[] = ":{$parameterKey}";
				$parameters[$parameterKey] = $value[$i];
			}
			$params = implode(", ", $paramsArray);
			$expression = "{$key} {$comparison} ({$params})";
		} else {
			$expression = "{$key}{$comparison}:{$parameterNamespace}{$key}_{$this->whereCounter}";
			$parameters = ["{$parameterNamespace}{$key}_{$this->whereCounter}" => $value];
		}

		$this->query->addWhere($this->whereConcatenator, $expression, $parameters);
		$this->whereCounter ++;

		return $this;
	}

	/**
	 * Execute where for raw SQL queries
	 *
	 * Builder::init()->select("book")->whereRaw("id IN (SELECT book_id FROM author WHERE SUBSTR(name, 0, 1)='A')");
	 * 
	 * @param string $expression The SQL expression for the where clause
	 * @return object Instance for chaining methods
	 */
	function whereRaw($expression, $parameters = null) {
		if ($parameters === null) {
			$parameters = [];
		}
		$this->query->addWhere($this->whereConcatenator, $expression, $parameters);
		return $this;
	}

	/**
	 * Values for insert and update clauses. If value is raw then pass in as an array.
	 *
	 *
	 * Builder::init()->insert("book")->values(["title" => "Funny Stories", "author" => "A. K. White"]);
	 * Builder::init()->insert("book")
	 * 					->values(["title" => "The Belgariad", "author" => "David Eddings"])
	 * 					->values(["title" => "Thief of time", "author" => "Terry Pratchett]);
	 * Builder::init()->update("book")->values(["title" => ["author"])->where("id", 35);
	 *
	 * @param $data array An associative array of key=>value pairs to insert
	 * @return object $this For chaining methods
	 */
	public function values($data) {
		$this->query->addValues($data);
		return $this;
	}

	/**
	 * Start a sub-where clause i.e scoped in brackets
	 *
	 * @return object $this For chaining methods
	 */
	function subWhere() {
		$this->query->pushWhereStack();
		return $this;
	}

	/**
	 * Ending a sub-where clause i.e scoped in brackets
	 *
	 * @return object $this For chaining methods
	 */
	function subWhereEnd() {
		$this->query->popWhereStack();
		return $this;
	}

	function hasWhere() {
		return $this->whereCounter > 0;
	}

	/**
	 * Group by an expressions
	 * 
	 * @return object $this For chaining methods
	 */
	public function groupBy($expression) {
		$this->query->setGroupBy($expression);
		return $this;
	}

	/**
	 * Having conditions for group by expressions
	 * 
	 * @return object $this For chaining methods
	 */
	public function having($expression) {
		$this->query->setHaving($expression);
		return $this;
	}

	/**
	 * Order by an expressions
	 */
	public function orderBy($expression) {
		$this->query->setOrderBy($expression);
		return $this;
	}

	/**
	 * On next concat of where clause conditions, using an 'OR'
	 * 
	 * @return object $this For chaining methods
	 */
	public function orCondition() {
		$this->whereConcatenator = "OR";
		return $this;
	}

	/**
	 * On next concat of where clause conditions, using an 'AND'
	 * 
	 * @return object $this For chaining methods
	 */
	public function andCondition() {
		$this->whereConcatenator = "AND";
		return $this;
	}

	/**
	 * Group by an expressions
	 */
	public function addParameters($parameters) {
		$this->query->addParameters($parameters);
		return $this;
	}

	/**
	 * Inner join
	 *
	 * Builder::init()->select("book")->innerJoin("author", "book_id=id");
	 * 
	 * @return object $this For chaining methods
	 */
	public function innerJoin($table, $expression) {
		$this->query->addJoin(new JoinExpression($table, JoinExpression::INNER_JOIN, JoinExpression::ON_CONDITION, $expression));
		return $this;
	}

	/**
	 * Left join
	 *
	 * Builder::init()->select("book")->leftJoin("author", "book_id=id");
	 * 
	 * @return object $this For chaining methods
	 */
	public function leftJoin($table, $expression) {
		$this->query->addJoin(new JoinExpression($table, JoinExpression::LEFT_JOIN, JoinExpression::ON_CONDITION, $expression));
		return $this;
	}

	/**
	 * Left outer join
	 *
	 * Builder::init()->select("book")->leftOuterJoin("author", "book_id=id");
	 * 
	 * @return object $this For chaining methods
	 */
	public function leftOuterJoin($table, $expression) {
		$this->query->addJoin(new JoinExpression($table, JoinExpression::LEFT_OUTER_JOIN, JoinExpression::ON_CONDITION, $expression));
		return $this;
	}

	public function getSql() {
		return $this->query->generateSql();
	}

	public function getParameters() {
		return $this->query->generateParameters();
	}
}
