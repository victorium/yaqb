<?php

/**
 * Yaqb/Query.php
 *
 * Copyright: (c) 2015 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yaqb;

use Yaqb\Expression\JoinExpression;

/**
 * Class that creates the query string and stores the parameters. It
 * contains the stacks that we append different portions of the query and keeps
 * track of parameters.
 */
class Query {

	const SELECT = 1;
	const INSERT = 2;
	const UPDATE = 3;
	const DELETE = 4;

	/** SELECT, INSERT, UPDATE or DELETE */
	protected $mode = null;

	protected $table = null;
	protected $action = null;
	protected $resultColumns = null;

	protected $from = "";

	protected $where = "";
	protected $whereScoped = 0;
	protected $whereStackCount = 0;

	protected $groupBy = "";

	protected $orderBy = "";

	protected $limit = "";

	protected $values = null;
	protected $defaultValues = false;
	protected $returningClause = null;
	protected $parameters = null;
	protected $insertColumns = null;

	protected $parameterNamespace = null;

	/**
	 * Get the generated SQL
	 *
	 * @return string The SQL
	 */
	public function __construct($parameterNamespace = "") {
		$this->parameterNamespace = $parameterNamespace;
		$this->values = [];
		$this->parameters = [];
		$this->whereScoped = false;
	}

	/**
	 * Get the generated SQL
	 *
	 * @return string The SQL
	 */
	public function generateSql() {
		switch ($this->mode) {
			case self::SELECT:
				$sql = "SELECT ";
				if ($this->action) {
					$sql .= "{$this->action} ";
				}
				if ($this->resultColumns === null) {
					$sql .= "* ";
				} else {
					$sql .= "{$this->resultColumns} ";
				}
				if ($this->table !== null) {
					$sql .= "FROM {$this->table}{$this->from}";
				}
				$sql .= "{$this->where}{$this->groupBy}{$this->orderBy}{$this->limit}";
				return trim($sql);
			case self::INSERT:
				$sql = "INSERT ";
				if ($this->action) {
					$sql .= "{$this->action} ";
				}
				$sql .= "INTO {$this->table}{$this->getInsertColumns()}VALUES {$this->getInsertValues()}";
				return $sql;
			case self::UPDATE:
				return trim("UPDATE {$this->table} SET {$this->getUpdateValues()}{$this->where}");
			case self::DELETE:
				return trim("DELETE FROM {$this->table} {$this->where}{$this->orderBy}{$this->limit}");
			default:
				return "";
		}
	}

	/**
	 * Get the parameters passed in as an associated array
	 *
	 * @return array The parameters used in the generated query
	 */
	public function generateParameters() {
		return $this->parameters;
	}

	/**
	 * Get the parameterNamespace
	 *
	 * @return string The parameters used in the generated query
	 */
	public function getParameterNamespace() {
		return $this->parameterNamespace;
	}

	/**
	 * Get the defaultValues
	 *
	 * @return boolean The defaultValues the insert values query is set to false
	 */
	public function getDefaultValues() {
		return $this->defaultValues;
	}

	/**
	 * Set the defaultValues, defaultValues is true by default
	 *
	 * @param bool $defaultValues If true the insert values query will have "DEFAULT VALUES" in values clause
	 */
	public function setDefaultValues($defaultValues) {
		$this->defaultValues = (bool) $defaultValues;
	}

	/**
	 * Set the defaultValues, defaultValues is true by default
	 *
	 * @param bool $defaultValues If true the insert values query will have "DEFAULT VALUES" in values clause
	 */
	public function setReturning($clause) {
		$this->returningClause = $clause;
	}

	/**
	 * Get the mode
	 *
	 * @return int The mode the query is set to
	 */
	public function getMode() {
		return $this->mode;
	}

	/**
	 * Set the mode
	 *
	 * @return array The parameters used in the generated query
	 */
	public function setMode($mode) {
		$this->mode = (int) $mode;
	}

	/**
	 * Set the table
	 *
	 * @param string $table The parameters used in the generated query
	 */
	public function setTable($table, $alias = null) {
		if ($alias !== null) {
			$table = "{$table} AS {$alias}";
		}
		$this->table = $table;
		$this->from = " ";
	}

	public function getTable() {
		return $this->table;
	}
	/**
	 * Add to the result columns
	 *
	 * @param string $result The result column list used in the select clause
	 */
	public function addResultColumns($column) {
		if ($this->resultColumns === null) {
			$this->resultColumns = "{$column}";
		} else {
			$this->resultColumns .= ", {$column}";
		}
	}

	/**
	 */
	public function setOrderBy($expression) {
		$this->orderBy = "ORDER BY {$expression} ";
	}

	/**
	 * @param string $expression Set the group by clause
	 */
	public function setGroupBy($expression) {
		$this->groupBy = "GROUP BY {$expression} ";
	}

	/**
	 * @param string $expression Set the having clause
	 */
	public function setHaving($expression) {
		if ($this->groupBy === "") {
			throw new \Exception("Missing group by clause before having keyword");
		}
		$this->groupBy .= "HAVING {$expression} ";
	}

	protected function getInsertColumns() {
		return " (" . implode(", ", $this->insertColumns) . ") ";
	}

	protected function getInsertValues() {
		$clause = [];

		if ($this->values) {
			$count = 0;
			if ($this->parameterNamespace) {
				$parameterNamespace = $this->parameterNamespace . "_";
			} else {
				$parameterNamespace = "";
			}

			foreach ($this->values as $row) {
				$subClause = [];
				foreach ($row as $key => $value) {
					if (is_array($value)) {
						$subClause[] = (string) $value[0];
					} else {
						$parameterKey = "{$parameterNamespace}{$key}_{$count}";
						$subClause[] = ":" . $parameterKey;
						$this->parameters[$parameterKey] = $value;
					}
				}
				$clause[] = "(" . implode(", ", $subClause) . ")";
				$count ++;
			}
		}

		$sql = implode(", ", $clause);

		if ($this->defaultValues) {
			$sql .= " DEFAULT VALUES";
		}

		if ($this->returningClause) {
			$sql .= " RETURNING {$this->returningClause}";
		}

		return $sql;
	}

	/**
	 * @return string UPDATE clause update clause values
	 */
	protected function getUpdateValues() {
		$clause = [];

		if (count($this->values) > 0) {
			if ($this->parameterNamespace) {
				$parameterNamespace = $this->parameterNamespace . "_";
			} else {
				$parameterNamespace = "";
			}
			$row = $this->values[0];

			foreach ($row as $key => $value) {
				if (is_array($value)) {
					$v = (string) $value[0];
					$clause[] = "{$key}={$v}";
				} else {
					$parameterKey = "{$parameterNamespace}{$key}";
					$clause[] = "{$key}=:{$parameterKey}";
					$this->parameters[$parameterKey] = $value;
				}
			}
		}

		$sql = implode(", ", $clause) . " ";

		return $sql;
	}

	/**
	 * @param mixed $expression The expression to use for the limit
	 */
	public function setLimit($expression) {
		$this->limit = "LIMIT {$expression}";
	}

	/**
	 * @param mixed $expression The expression to use for the offset
	 */
	public function setOffset($expression) {
		if ($this->limit === "") {
			throw new \Exception("Missing limit clause before offset keyword");
		}
		$this->limit .= " OFFSET {$expression}";;
	}

	/**
	 * Adds join expression to current where scope.
	 */
	public function addJoin(JoinExpression $expression) {
		if ($this->from === "") {
			throw new \Exception("Join can't come before setting table");
		}
		$this->from .= "{$expression->sql} ";
	}

	/**
	 * Adds values to the insert/update values expression
	 */
	public function addValues(array $values) {
		if (count($this->values) == 0) {
			$this->insertColumns = array_keys($values);
			$this->values[] = $values;
		} else {
			$insert = [];
			foreach ($this->insertColumns as $column) {
				if (array_key_exists($column, $values)) {
					$insert[$column] = $values[$column];
				}
			}
			$this->values[] = $insert;
		}
	}

	/**
	 * Adds where expression to current where scope.
	 */
	public function addWhere($concatenator, $expression, $parameters) {
		$this->parameters = array_merge($this->parameters, $parameters);
		if ($this->where === "") {
			$this->where = "WHERE ";
			$this->whereStackCount = 0;
		}
		if ($this->whereStackCount > 0) {
			$this->where .= " {$concatenator} ";
		}
		if ($this->whereScoped) {
			$this->where .= "(";
			$this->whereScoped = false;
		}
		$this->where .= $expression;
		$this->whereStackCount ++;
	}

	/**
	 * Adds where expression to current where scope.
	 */
	public function addParameters($parameters) {
		$this->parameters = array_merge($parameters, $this->parameters);
	}

	/**
	 * Creates a new where stack. For multiple scoped where expressions.
	 */
	public function pushWhereStack() {
		if ($this->where === "") {
			$this->where = "WHERE (";
			$this->whereStackCount = 0;
		} else {
			$this->whereScoped = true;
		}
	}

	/**
	 * Moves to previous where stack.
	 */
	public function popWhereStack() {
		$this->where .= ")";
	}
	
	/**
	 * Set extra directives directly after select and insert keywords
	 */
	public function setAction($action) {
		$this->action = $action;
	}
}
