<?php

/**
 * Yaqb/Expression/JoinExpression.php
 *
 * Copyright: (c) 2015 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yaqb\Expression;

/**
 * JoinExpression class. Simple class for single join expression in join clause.
 */
class JoinExpression {
	const LEFT_JOIN = 1;
	const LEFT_OUTER_JOIN = 2;
	const INNER_JOIN = 3;

	const ON_CONDITION = 1;
	const USING_CONDITION = 2;

	public $sql = "";

	/**
	 * @param string $table The name of the table
	 * @param int $operator LEFT_JOIN, LEFT_OUTER_JOIN, INNER_JOIN options
	 * @param int $condition ON_CONDITION, USING_CONDITION options
	 * @param string $expression The expression to use containing the
	 */
	public function __construct($table, $operator, $condition, $expression) {
		$this->sql = "";

		switch ($operator) {
			case self::LEFT_JOIN: $this->sql .= "LEFT JOIN "; break;
			case self::LEFT_OUTER_JOIN: $this->sql .= "LEFT OUTER JOIN "; break;
			case self::INNER_JOIN: $this->sql .= "INNER JOIN "; break;
		}

		switch ($condition) {
			case self::ON_CONDITION: $this->sql .= "{$table} ON {$expression} "; break;
			case self::USING_CONDITION: $this->sql .= "{$table} USING ({$expression}) "; break;
		}
	}
}
