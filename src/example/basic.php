<?php

define("PATH_TO_YAQB_SRC", dirname(__DIR__));

require PATH_TO_YAQB_SRC . "/bootstrap.php";

$builder = \Yaqb\Builder::init();
$builder->select("pointer")->where("id", 4); // start a simple select statement

echo $builder->getSql(), PHP_EOL; // get the SQL
print_r($builder->getParameters()); // get the SQL parameters
